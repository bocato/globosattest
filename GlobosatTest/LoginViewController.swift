//
//  LoginViewController.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SkyFloatingLabelTextField
import SnapKit

private enum SegueIdentifiers: String {
    case Signup = "SignupSegue"
}

protocol LoginViewControllerDelegate {
    func successfullyLoggedInWithUser(_ user: User)
}

class LoginViewController: UIViewController {

    // MARK: - Properties
    var delegate: LoginViewControllerDelegate?
    
    // MARK: - View Elements
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    // MARK: - Instantiation
    static func instantiateNew() ->  LoginViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        return controller
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFieldsAcessoryView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    // MARK: - Configuration
    func configureTextFieldsAcessoryView() {
        let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        toolbar.barStyle = .default
        
        let cancelButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissKeyboard))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let signInButtonItem = UIBarButtonItem(title: "Login", style: .done, target: self, action: #selector(loginAction))
        
        toolbar.items = [cancelButtonItem, flexibleSpace, signInButtonItem]
        toolbar.sizeToFit()
        
        self.emailTextField.inputAccessoryView = toolbar
        self.passwordTextField.inputAccessoryView = toolbar
    }
    
    // MARK: - Validations
    func getValidatedEmail() -> (isValid: Bool, value: String?) {
        guard let email = emailTextField.text, !email.isEmpty && email.isValidEmail else {
            emailTextField.errorMessage = "Invalid email."
            return (false, nil)
        }
        return (true, email)
    }
    
    func getValidatedPassword() -> (isValid: Bool, value: String?) {
        guard let password = passwordTextField.text, !password.isEmpty else {
            passwordTextField.errorMessage = "Invalid password."
            return (false, nil)
        }
        return (true, password)
    }
    
    // MARK: - Service Calls
    func doLogin(withEmail validatedEmail: (isValid: Bool, value: String?), password validatedPassword: (isValid: Bool, value: String?)) {
        
        guard let email = validatedEmail.value, let password = validatedPassword.value, validatedEmail.isValid && validatedPassword.isValid else {
            return
        }
        
        SwiftyLoadingView.show(in: self, withText: "Loging in...", background: true)
        LoginService.doLogin(withEmail: email, password: password, success: { (user) in
            guard let user = user else {
                AlertHelper.showAlert(in: self, with: "Error", message: ErrorMessages.unexpected.rawValue)
                return
            }
            self.handle(user: user)
        }, failure: { (error) in
            AlertHelper.showAlert(in: self, with: "Error", message: error?.localizedDescription ?? ErrorMessages.unknown.rawValue)
        }, completion: {
            SwiftyLoadingView.hide(for: self)
        })
    }
    
    // MARK: - Handle User
    func handle(user: User, completion: (() -> ())? = nil) {
        dismiss(animated: true) { 
            self.delegate?.successfullyLoggedInWithUser(user)
        }
    }
    
    // MARK: Actions
    func loginAction(){
        doLogin(withEmail: self.getValidatedEmail(), password: self.getValidatedPassword())
    }
    
    func dismissKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    // MARK: - Button Actions
    @IBAction func loginButtonDidTouchUpInside(_ sender: Any) {
        loginAction()
    }
    
    @IBAction func dismissButtonDidReceiveTouchUpInside(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.Signup.rawValue {
            let destination = segue.destination as! SignupViewController
            destination.delegate = self
        }
    }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let skyFloatingLabelTextField = textField as? SkyFloatingLabelTextField {
            skyFloatingLabelTextField.errorMessage = nil
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

// MARK: - SignupViewControllerDelegate
extension LoginViewController: SignupViewControllerDelegate {
    
    func successfullySignedUpWithUser(_ user: User) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
