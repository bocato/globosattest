//
//  LoadingView.h
//
//  Created by Eduardo Sanches Bocato on 17/06/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoadingView;

#pragma mark - Delegate
@protocol LoadingViewDelegate <NSObject>
@optional
- (void)showLoadingView:(LoadingView *)loadingView;
- (void)hideLoadingView:(LoadingView *)loadingView;
@end

@interface LoadingView : UIControl

#pragma mark - Properties
@property (strong, nonatomic) id<LoadingViewDelegate> delegate;
@property (nonatomic, assign) BOOL showBackgroundView;

#pragma mark - View Behavior
- (void)hide;
- (void)show;

#pragma mark - Class Methods
+ (void)showInView:(UIView *)view;
+ (void)showInView:(UIView *)view withLoaderStyle:(UIActivityIndicatorViewStyle)style;
+ (void)hideForView:(UIView *)view;

@end
