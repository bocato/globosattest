//
//  SimpleMovieTableViewCell.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 13/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class SimpleMovieTableViewCell: UITableViewCell {

    // MARK: - View Elements
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieInfoLabel: UILabel!
    
    // MARK: - Lifecycle
    override func prepareForReuse() {
        super.prepareForReuse()
        self.posterImageView.image = UIImage(named: "placeholder_movie")
    }

    // MARK: - Configuration
    func loadPosterWithURLString(_ urlString: String!) {
        LoadingView.show(in: posterImageView, withLoaderStyle: .gray)
        Alamofire.request(urlString).responseImage { (response) in
            guard let image = response.result.value else {
                self.posterImageView.image = UIImage(named: "placeholder_movie")
                LoadingView.hide(for: self.posterImageView)
                return
            }
            self.posterImageView.image = image
            LoadingView.hide(for: self.posterImageView)
        }
    }
    
    func configureWithMovie(_ movie: Movie?) {
        if let imageURLString = movie?.poster/*, imageURLString.isValidURL*/ {
           loadPosterWithURLString(imageURLString)
        }
        
        if let title = movie?.title {
            let attributedText = NSMutableAttributedString(string: title, attributes: [NSForegroundColorAttributeName: UIColor.black])
            if  let year = movie?.year, !year.isEmpty {
                attributedText.append(NSAttributedString(string: " (\(year))", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            }
            movieInfoLabel.attributedText = attributedText
        }
        
    }
    
}
