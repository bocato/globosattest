//
//  UICollectionViewExtension.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit

public extension UICollectionView {
    
    var centerCellIndexPath: IndexPath? {
        var closestCellFromCenter = self.visibleCells[0]
        for cell in self.visibleCells {
            let closestCellDelta = fabs(closestCellFromCenter.center.x - self.bounds.size.width/2 - self.contentOffset.x)
            let cellDelta = fabs(cell.center.x - self.bounds.size.width/2 - self.contentOffset.x)
            if cellDelta < closestCellDelta {
                closestCellFromCenter = cell
            }
        }
        return indexPath(for: closestCellFromCenter)
    }

}
