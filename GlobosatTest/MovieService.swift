//
//  MovieService.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit

class MovieService: GenericService {

    class func getDetailsForMovie(withId id: String, onSuccess success: @escaping ((Movie?) -> Void), onFailure failure: @escaping((NSError?) -> Void), onCompletion completion: (() -> Void)? = nil) {
        guard let url = URLFactory.OMDBApi.createURLForMovieDetails(with: id) else {
            return
        }
        self.request(.get, url, onSuccess: success, onFailure: failure, onCompletion: completion)
    }
    
    class func searchMovies(withTitle title: String, page: Int? = nil, onSuccess success: @escaping ((MovieSearch?) -> Void), onFailure failure: @escaping((NSError?) -> Void), onCompletion completion: (() -> Void)? = nil) {
        let urlEncondedTitle = title.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        guard let url = URLFactory.OMDBApi.createURLForMovieSearch(with: urlEncondedTitle!, page: page) else {
            return
        }
        self.request(.get, url, onSuccess: success, onFailure: failure, onCompletion: completion)
    }
    
}
