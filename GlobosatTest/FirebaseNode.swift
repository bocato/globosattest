//
//  FirebaseNode.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 16/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation

enum FirebaseNode: String {
    case Favorites = "favorites"
}
