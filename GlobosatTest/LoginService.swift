//
//  LoginService.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class LoginService {
    
    // MARK: Requests
    static func doLogin(withEmail email: String!, password: String!, success: @escaping (User!) -> (), failure: @escaping (Error?) -> (), completion: (() -> ())? = nil) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if let error = error {
                failure(error as NSError)
                completion?()
            }
            if let user = user {
                success(user)
                completion?()
            }
        })
    }
    
    static func doLogout(success: @escaping (User!) -> (), failure: @escaping (Error?) -> (), completion: (() -> ())? = nil) {
        if let currentUser = Auth.auth().currentUser {
            do {
                try Auth.auth().signOut()
                success(currentUser)
            } catch let error {
                failure(error as NSError)
            }
        }
        completion?()
    }
    
    static func resetPassword(withEmail email: String!, success: @escaping () -> (), failure: @escaping (Error?) -> (), completion: (() -> ())? = nil) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let error = error {
                failure(error as NSError)
            }
            else {
                success()
            }
            completion?()
        }
    }
    
}
