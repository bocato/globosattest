//
//  FavoritesService.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 16/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class FavoritesService {

    static func recordNodeFor(_ user: User!, imdbId: String!) ->  DatabaseReference {
        return Database.database().reference().child(FirebaseNode.Favorites.rawValue).child(user.uid).child(imdbId)
    }
    
    static func saveMovie(_ movie: Movie?, success: @escaping (Movie!) -> (), failure: @escaping (NSError?) -> (), completion: (() -> ())? = nil) {
        if let movie = movie {
            if let uid = Auth.auth().currentUser?.uid, let imdbId = movie.imdbId  {
                let currentUserNodeOnDatabase = Database.database().reference().child(FirebaseNode.Favorites.rawValue).child(uid)
                let newFavoriteNode = currentUserNodeOnDatabase.child(imdbId)
                newFavoriteNode.observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.exists() {
                        failure(NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.duplicatedDatabaseEntry.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.duplicatedDatabaseEntry.rawValue]))
                    }
                    else {
                        for (key, value) in movie.toJSON() {
                            newFavoriteNode.child(key).setValue(value)
                        }
                        movie.firebaseReference = newFavoriteNode.ref
                        success(movie)
                    }
                })
            }
            else {
                failure(NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.userIsNotLoggedIn.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.userIsNotLoggedIn.rawValue]))
            }
        }
        else {
            failure(NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.invalidParameters.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.invalidParameters.rawValue]))
        }
        completion?()
        
    }
    
    // TODO: REFACTOR...
    static func delete(_ movie: Movie?, success: @escaping (Movie!) -> (), failure: @escaping (NSError?) -> (), completion: (() -> ())? = nil) {
        
        if let movie = movie {
            if let uid = Auth.auth().currentUser?.uid, let imdbId = movie.imdbId  {
                let currentUserNodeOnDatabase = Database.database().reference().child(FirebaseNode.Favorites.rawValue).child(uid)
                let movieNodeToDelete = currentUserNodeOnDatabase.child(imdbId)
                movieNodeToDelete.observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.exists() {
                        movieNodeToDelete.removeValue(completionBlock: { (error, databaseReference) in
                            if error != nil {
                                failure(error! as NSError)
                            }
                            else {
                                success(movie)
                            }
                            completion?()
                        })
                    }
                    else {
                        failure(NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.invalidParameters.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.invalidParameters.rawValue]))
                        completion?()
                    }
                    
                })
            }
            else {
                failure(NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.userIsNotLoggedIn.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.userIsNotLoggedIn.rawValue]))
                completion?()
            }
            
        }
        else {
            failure(NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.invalidParameters.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.invalidParameters.rawValue]))
            completion?()
            
        }
        
    }
    
    static func getAllFavorites(success: @escaping ([Movie]?) -> (), failure: @escaping (NSError?) -> (), completion: (() -> ())? = nil) {
        
        if let uid = Auth.auth().currentUser?.uid  {
            Database.database().reference().child(FirebaseNode.Favorites.rawValue).child(uid).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    var movies = [Movie]()
                    for item in snapshot.children {
                        let user = Movie(snapshot: item as! DataSnapshot)
                        movies.append(user)
                    }
                    success(movies)
                }
                else  {
                    failure(NSError(domain: ErrorDomain.local.rawValue, code: 404, userInfo: [NSLocalizedDescriptionKey: ErrorMessages.favoritesListNotFound.rawValue]))
                }
                completion?()
            }, withCancel: { (error) in
                failure(error as NSError)
            })
        }
        else {
            failure(NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.userIsNotLoggedIn.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.userIsNotLoggedIn.rawValue]))
        }
        completion?()
        
    }
    
}
