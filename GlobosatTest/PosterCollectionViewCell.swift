//
//  PosterCollectionViewCell.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 16/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

protocol PosterCollectionViewCellDelegate {
    func imageViewDidReceiveTap(_ imageView: UIImageView, on cell: PosterCollectionViewCell)
}

class PosterCollectionViewCell: UICollectionViewCell {
    
    // MARK: - View Elements
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    // MARK: - Properties
    var delegate: PosterCollectionViewCellDelegate?
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        posterImageView.isUserInteractionEnabled = true
        posterImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        posterImageView.image = UIImage(named: "placeholder_movie")
        movieTitleLabel.text = ""
    }
    
    // MARK: - Configuration
    func loadPosterWithURLString(_ urlString: String!) {
        LoadingView.show(in: posterImageView, withLoaderStyle: .gray)
        Alamofire.request(urlString).responseImage { (response) in
            guard let image = response.result.value else {
                self.posterImageView.image = UIImage(named: "placeholder_movie")
                LoadingView.hide(for: self.posterImageView)
                return
            }
            self.posterImageView.image = image
            LoadingView.hide(for: self.posterImageView)
        }
    }
    
    func configureWithMovie(_ movie: Movie?) {
        if let imageURLString = movie?.poster/*, imageURLString.isValidURL*/ {
            loadPosterWithURLString(imageURLString)
        }
        
        if let title = movie?.title {
            let attributedText = NSMutableAttributedString(string: title, attributes: [NSForegroundColorAttributeName: UIColor.black])
            if  let year = movie?.year, !year.isEmpty {
                attributedText.append(NSAttributedString(string: "\n(\(year))", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            }
            movieTitleLabel.attributedText = attributedText
        }
        
    }
    
    // MARK: - Actions
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = tapGestureRecognizer.view as! UIImageView
        self.delegate?.imageViewDidReceiveTap(tappedImageView, on: self)
    }
    
}
