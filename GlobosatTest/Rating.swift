//
//  Rating.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import ObjectMapper

class Rating: Mappable {
    
    var source: String?
    var value: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        source <- map["Source"]
        value <- map["Value"]
    }
    
}
