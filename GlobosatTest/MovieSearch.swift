//
//  MovieSearch.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import ObjectMapper
import FirebaseDatabase

class MovieSearch: Mappable {
    
    // MARK: - Properties
    var firebaseReference: DatabaseReference? = nil
    var search: [Movie]?
    var totalResults: String?
    var response: String?
    
        // MARK: - Initialization
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        search <- map["Search"]
        totalResults <- map["totalResults"]
        response <- map["Response"]
    }
    
    required init(snapshot: DataSnapshot) {
        firebaseReference = snapshot.ref
        if let firebaseJson = snapshot.value as? [String: Any] {
            mapping(map: Map(mappingType: .fromJSON, JSON: firebaseJson))
        }
    }
    
}
