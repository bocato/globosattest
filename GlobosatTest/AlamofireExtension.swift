//
//  AlamofireExtension.swift
//
//  Created by Eduardo Sanches Bocato on 17/06/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol ResponseObjectSerializable {
    init?(response: HTTPURLResponse, representation: Any)
}

enum BackendError: Error {
    case network(error: Error) // Capture any underlying Error from the URLSession API
    case dataSerialization(error: Error)
    case jsonSerialization(error: Error)
    case xmlSerialization(error: Error)
    case objectSerialization(reason: String)
}

public extension DataRequest {
    
    // MARK: - Serialization
    @discardableResult func serializedObject<SuccessObjectType: Mappable>(onSuccess success: @escaping  ((SuccessObjectType?) -> Void),
                                             onFailure failure: @escaping ((NSError?) -> Void),
                                             onCompletion completion: (() -> Void)? = nil) -> Self {
        
        let responseSerializer = DataResponseSerializer<SuccessObjectType> { request, response, data, error in
            
            let requestSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = requestSerializer.serializeResponse(request, response, data, error)
            
            
            if let statusCode = response?.statusCode {
                
                switch statusCode {
                    
                case 200...299:
                    if let serializedValue = Mapper<SuccessObjectType>().map(JSONObject: result.value) {
                        return .success(serializedValue)
                    }
                    else {
                        let newError = NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.objectMapperParse.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.objectMapperParse.rawValue])
                        return .failure(newError)
                    }
                    
                    
                case 404:
                    
                    let newError = NSError(domain: ErrorDomain.external.rawValue, code: statusCode, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.notFound.rawValue])
                    return .failure(newError)
                    
                default:
                    
                    let newError = NSError(domain: ErrorDomain.external.rawValue, code: statusCode, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.unknown.rawValue])
                    return .failure(newError)
                    
                }
            } else {
                let newError = NSError(domain: ErrorDomain.external.rawValue, code: CustomErrorCode.unknow.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.unknown.rawValue])
                
                return .failure(newError)
            }
            
        }
        
        return response(queue: nil, responseSerializer: responseSerializer, completionHandler: { (response) in
            switch response.result {
            case .success:
                success(response.result.value)
                
            case .failure(let error):
                failure(error as NSError?)
            }
            
            if let unwrappedCompletion = completion {
                unwrappedCompletion()
            }
        })
    }
    
    @discardableResult func serializedArray<SuccessObjectType: Mappable>(_ keyPath: String? = nil, onSuccess success: @escaping  (([SuccessObjectType]?) -> Void),
                                            onFailure failure: @escaping ((NSError?) -> Void),
                                            onCompletion completion: (() -> Void)? = nil) -> Self {
        
        let responseSerializer = DataResponseSerializer<[SuccessObjectType]> { request, response, data, error in
            
            let requestSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = requestSerializer.serializeResponse(request, response, data, error)
            
            if let statusCode = response?.statusCode {
                
                switch statusCode {
                    
                case 200...299:
                    
                    if let value = result.value {
                        
                        var JSONToMap = result.value as Any?
                        if let keyPath = keyPath, keyPath.isEmpty == false {
                            JSONToMap = (value as AnyObject?)?.value(forKeyPath: keyPath)
                            
                        } else {
                            JSONToMap = value as AnyObject?
                        }
                        
                        if let serializedValue = Mapper<SuccessObjectType>(context: nil, shouldIncludeNilValues: false).mapArray(JSONObject: JSONToMap) {
                            
                            return .success(serializedValue)
                        }
                        else {
                            
                            let newError = NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.objectMapperParse.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.objectMapperParse.rawValue])
                            failure(newError)
                            
                            return .failure(newError)
                        }
                    }
                    else {
                        
                        let newError = NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.objectMapperParse.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.objectMapperParse.rawValue])
                        failure(newError)
                        
                        return .failure(newError)
                        
                        
                    }
                case 404:
                    
                    let newError = NSError(domain: ErrorDomain.external.rawValue, code: statusCode, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.notFound.rawValue])
                    failure(newError)
                    
                    return .failure(newError)
                    
                default:
                    
                    let newError = NSError(domain: ErrorDomain.external.rawValue, code: statusCode, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.unexpected.rawValue])
                    return .failure(newError)
                    
                }
            } else {
                
                let newError = NSError(domain: ErrorDomain.external.rawValue, code: CustomErrorCode.unknow.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.unknown.rawValue])
                
                return .failure(newError)
                
            }
        }
        
        return response(queue: nil, responseSerializer: responseSerializer, completionHandler: { (response) in
            switch response.result {
            case .success:
                success(response.result.value)
                
            case .failure(let error):
                failure(error as NSError?)
            }
            
            if let unwrappedCompletion = completion {
                unwrappedCompletion()
            }
        })
        
    }
    
    @discardableResult func emptyObject(onSuccess success: @escaping  () -> Void,
                                        onFailure failure: @escaping ((NSError?) -> Void),
                                        onCompletion completion: (() -> Void)? = nil) -> Self {
        let responseSerializer = DataResponseSerializer<Void> { request, response, data, error in
            
            
            if let contentLenght = response?.allHeaderFields["Content-Length"] as? String, Int(contentLenght)! > 0 {
                let requestSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
                _ = requestSerializer.serializeResponse(request, response, data, error)
            }
            
            
            if let statusCode = response?.statusCode {
                switch statusCode {
                    
                case 200...299:
                    
                    return .success()
                    
                case 404:
                    let newError = NSError(domain: ErrorDomain.external.rawValue, code: statusCode, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.notFound.rawValue])
                    return .failure(newError)
                    
                    
                default:
                    
                    let newError = NSError(domain: ErrorDomain.external.rawValue, code: statusCode, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.unknown.rawValue])
                    return .failure(newError)
                    
                    
                }
            } else {
                
                let newError = NSError(domain: ErrorDomain.external.rawValue, code: CustomErrorCode.unknow.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.unknown.rawValue])
                
                return .failure(newError)
                
            }
        }
        
        return response(queue: nil, responseSerializer: responseSerializer, completionHandler: { (response) in
            switch response.result {
            case .success:
                success()
                
            case .failure(let error):
                failure(error as NSError?)
            }
            
            if let unwrappedCompletion = completion {
                unwrappedCompletion()
            }
        })
    }
    
    // MARK: - Reachability
    func checkReachability() -> NSError? {
        
        guard let url = self.request?.url, let hostURL = self.cleanHostURL(url) else {
            return NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.noURLParameters.rawValue, userInfo: [
                kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorDomain.local.rawValue,
                kCFErrorLocalizedFailureReasonKey as AnyHashable : ErrorMessages.noURLParameters.rawValue])
        }
        
        if let networkReachabilityManager = Alamofire.NetworkReachabilityManager(host: hostURL), !networkReachabilityManager.isReachable {
            return NSError(domain: ErrorDomain.local.rawValue, code: CustomErrorCode.couldNotConnectToHost.rawValue, userInfo: [kCFErrorLocalizedDescriptionKey as AnyHashable : ErrorMessages.networkFailure.rawValue, kCFURLErrorFailingURLErrorKey as AnyHashable: "\(ErrorMessages.couldNotConnectToHost.rawValue)\(url)"])
        }
        
        return nil
    }
    
    private func cleanHostURL(_ url: URL) -> String? {
        guard var hostURL = NSURL(string: "/", relativeTo: url)?.absoluteString else {
            return nil
        }
        hostURL = hostURL.replacingOccurrences(of: "http://", with: "")
        hostURL = hostURL.replacingOccurrences(of: "https://", with: "")
        
        return hostURL
    }
    
}
