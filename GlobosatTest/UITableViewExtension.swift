//
//  UITableViewExtension.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit

public extension UITableView {
    var tableViewScrollDidReachBottom: Bool {
        return self.contentOffset.y >= (self.contentSize.height - self.frame.size.height)
    }
}
