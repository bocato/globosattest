//
//  GenericService.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

private let GlobosatTestService: SessionManager = {
    var serverTrustPolicies: [String: ServerTrustPolicy]
    var url = NSURL(string: URLFactory.omdbAPIBaseURL)!
    
    serverTrustPolicies = [url.host!: .performDefaultEvaluation(validateHost: true)]
    
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    configuration.timeoutIntervalForRequest = 10.0
    
    return SessionManager(
        configuration: configuration,
        serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
    )
}()

private let reachabilityManager: Alamofire.NetworkReachabilityManager = {
    let manager = Alamofire.NetworkReachabilityManager(host: URLFactory.omdbAPIBaseURL)!
    manager.startListening()
    return manager
}()

protocol GenericService {
    @discardableResult static func request<SuccessObjectType: Mappable>(
        _ method: HTTPMethod,
        _ URLString: URLConvertible,
        parameters: Parameters?,
        encoding: ParameterEncoding,
        headers: HTTPHeaders?,
        onSuccess success: @escaping ((SuccessObjectType?) -> Void),
        onFailure failure: @escaping  ((NSError?) -> Void),
        onCompletion completion: (() -> Void)?)
        -> Request
    
    @discardableResult static func requestArrayForKeyPath<SuccessObjectType: Mappable>(
        _ keyPath: String,
        method: HTTPMethod,
        _ URLString: URLConvertible,
        parameters: Parameters?,
        encoding: ParameterEncoding,
        headers: HTTPHeaders?,
        onSuccess success: @escaping (([SuccessObjectType]?) -> Void),
        onFailure failure: @escaping  ((NSError?) -> Void),
        onCompletion completion: (() -> Void)?)
        -> Request
    
    @discardableResult static func requestEmptyObject(
        _ method: HTTPMethod,
        _ URLString: URLConvertible,
        parameters: Parameters?,
        encoding: ParameterEncoding,
        headers: HTTPHeaders?,
        onSuccess success: @escaping (() -> Void),
        onFailure failure: @escaping  ((NSError?) -> Void),
        onCompletion completion: (() -> Void)?)
        -> Request
}

extension GenericService {
    
    @discardableResult static func request<SuccessObjectType: Mappable>(
        _ method: HTTPMethod = .get,
        _ URLString: URLConvertible,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil,
        onSuccess success: @escaping ((SuccessObjectType?) -> Void),
        onFailure failure: @escaping  ((NSError?) -> Void),
        onCompletion completion: (() -> Void)? = nil)
        -> Request {
            
            debugPrint("request url = \(URLString)")
            debugPrint("parameters = \(String(describing: parameters))")
            debugPrint("encoding = \(encoding)")
            debugPrint("headers = \(String(describing: headers))")
            
            let request = GlobosatTestService.request(URLString, method: method, parameters: parameters, encoding: encoding, headers: headers)
            
            if let reachabilityError = request.checkReachability() {
                failure(reachabilityError)
                if let completion = completion {
                    completion()
                }
                return request
            }
            
            request.serializedObject(onSuccess: success, onFailure: failure, onCompletion: completion)
            
            return request
    }
    
    @discardableResult static func requestArrayForKeyPath<SuccessObjectType: Mappable>(
        _ keyPath: String,
        method: HTTPMethod = .get,
        _ URLString: URLConvertible,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders?  = nil ,
        onSuccess success: @escaping (([SuccessObjectType]?) -> Void),
        onFailure failure: @escaping  ((NSError?) -> Void),
        onCompletion completion: (() -> Void)? = nil)
        -> Request {
            
            debugPrint("request url = \(URLString)")
            debugPrint("parameters = \(String(describing: parameters))")
            debugPrint("encoding = \(encoding)")
            debugPrint("headers = \(String(describing: headers))")
            
            let request = GlobosatTestService.request(URLString, method: method, parameters: parameters, encoding: encoding, headers: headers)
            
            if let reachabilityError = request.checkReachability() {
                failure(reachabilityError)
                if let completion = completion {
                    completion()
                }
                return request
            }
            
            request.serializedArray(keyPath, onSuccess: success, onFailure: failure, onCompletion: completion)
            
            
            return request
    }
    
    @discardableResult static func requestEmptyObject(
        _ method: HTTPMethod = .get,
        _ URLString: URLConvertible,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil,
        onSuccess success: @escaping (() -> Void),
        onFailure failure: @escaping  ((NSError?) -> Void),
        onCompletion completion: (() -> Void)? = nil)
        -> Request {
            
            debugPrint("request url = \(URLString)")
            debugPrint("parameters = \(String(describing: parameters))")
            debugPrint("encoding = \(encoding)")
            debugPrint("headers = \(String(describing: headers))")
            
            let request = GlobosatTestService.request(URLString, method: method, parameters: parameters, encoding: encoding, headers: headers)
            
            if let reachabilityError = request.checkReachability() {
                failure(reachabilityError)
                if let completion = completion {
                    completion()
                }
                return request
            }
            
            _ = request.emptyObject(onSuccess: success, onFailure: failure, onCompletion: completion)
            
            return request
    }
    
}

