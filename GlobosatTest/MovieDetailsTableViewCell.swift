//
//  MovieDetailsTableViewCell.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

protocol MovieDetailsTableViewCellDelegate {
    func websiteButtonDidReceiveTouchUpInside(in cell: MovieDetailsTableViewCell)
}

class MovieDetailsTableViewCell: UITableViewCell {

    // MARK: - View Elements
    @IBOutlet weak var titleAndDateLabel: UILabel!
    @IBOutlet weak var aditionalInfoLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var imdbRatingLabel: UILabel!
    @IBOutlet weak var imdbVotesLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var actorsLabel: UILabel!
    @IBOutlet weak var productionLabel: UILabel!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var plotTextView: UITextView!
    
    // MARK: - View Properties
    var delegate: MovieDetailsTableViewCellDelegate?
    
    // MARK: - Lifecycle
    override func prepareForReuse() {
        super.prepareForReuse()
        titleAndDateLabel.text = "-"
        aditionalInfoLabel.text = "-"
        posterImageView.image = UIImage(named: "placeholder_movie")
        imdbRatingLabel.text = "-"
        imdbVotesLabel.text = "-"
        imdbVotesLabel.text = "-"
        directorLabel.text = "-"
        writerLabel.text = "-"
        actorsLabel.text = "-"
        productionLabel.text = "-"
        plotTextView.text = "-"
    }
    
    // MARK: - Configuration
    func loadPosterWithURLString(_ urlString: String!) {
        LoadingView.show(in: posterImageView, withLoaderStyle: .gray)
        Alamofire.request(urlString).responseImage { (response) in
            guard let image = response.result.value else {
                self.posterImageView.image = UIImage(named: "placeholder_movie")
                LoadingView.hide(for: self.posterImageView)
                return
            }
            self.posterImageView.image = image
            LoadingView.hide(for: self.posterImageView)
        }
    }
    
    func configureWithMovie(_ movie: Movie?) {
        
        if let title = movie?.title {
            let attributedText = NSMutableAttributedString(string: title, attributes: [NSForegroundColorAttributeName: UIColor.black])
            if  let year = movie?.year, !year.isEmpty {
                attributedText.append(NSAttributedString(string: " (\(year))", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            }
            titleAndDateLabel.attributedText = attributedText
        }
        
        if let rated = movie?.rated, let runtime = movie?.runtime, let genre = movie?.genre {
            aditionalInfoLabel.text = "\(rated) | \(runtime) | \(genre)"
        }
        
        if let imageURLString = movie?.poster/*, imageURLString.isValidURL*/ {
            loadPosterWithURLString(imageURLString)
        }
        
        if let imdbRating = movie?.imdbRating {
            imdbRatingLabel.text = "\(imdbRating)/10"
        }
        
        if let imdbVotes = movie?.imdbVotes {
            imdbVotesLabel.text = imdbVotes
        }
        
        if let director = movie?.director, !director.isEmpty {
            directorLabel.isHidden = false
            let attributedText = NSMutableAttributedString(string: "Director: ", attributes: [NSForegroundColorAttributeName: UIColor.black])
            attributedText.append(NSAttributedString(string: "\(director)", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            directorLabel.attributedText = attributedText
        }
        
        if let writer = movie?.writer, !writer.isEmpty {
            writerLabel.isHidden = false
            let attributedText = NSMutableAttributedString(string: "Writer: ", attributes: [NSForegroundColorAttributeName: UIColor.black])
            attributedText.append(NSAttributedString(string: "\(writer)", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            writerLabel.attributedText = attributedText
        }
        
        if let actors = movie?.actors, !actors.isEmpty {
            actorsLabel.isHidden = false
            let attributedText = NSMutableAttributedString(string: "Actors: ", attributes: [NSForegroundColorAttributeName: UIColor.black])
            attributedText.append(NSAttributedString(string: "\(actors)", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            actorsLabel.attributedText = attributedText
        }
        
        if let production = movie?.production, !production.isEmpty {
            productionLabel.isHidden = false
            let attributedText = NSMutableAttributedString(string: "Production: ", attributes: [NSForegroundColorAttributeName: UIColor.black])
            attributedText.append(NSAttributedString(string: "\(production)", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            productionLabel.attributedText = attributedText
        }
        
        if let website = movie?.website, !website.isEmpty {
            websiteButton.isHidden = false
            let attributedText = NSMutableAttributedString(string: "Website: ", attributes: [NSForegroundColorAttributeName: UIColor.black])
            attributedText.append(NSAttributedString(string: "\(website)", attributes: [NSForegroundColorAttributeName: UIColor.darkGray]))
            websiteButton.setAttributedTitle(attributedText, for: .normal)
        }
        
        if let plot = movie?.plot {
            plotTextView.text = plot
        }
        
    }
    
    // MARK: Button Actions
    @IBAction func websiteButtonDidReceiveTouchUpInside(_ sender: Any) {
        self.delegate?.websiteButtonDidReceiveTouchUpInside(in: self)
    }
    
}
