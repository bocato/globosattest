//
//  URLFactory.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation

// Since I don't have other environments, this enum will suffice
private enum Defaults: String {
    case omdbAPIBaseURL = "https://www.omdbapi.com/"
    case posterAPIBaseURL = "http://www.img.omdbapi.com/"
    case apiKey = "f77f7f85"
}

class URLFactory {
    
    static var omdbAPIBaseURL: String {
        return "\(Defaults.omdbAPIBaseURL.rawValue)?apikey=\(Defaults.apiKey.rawValue)"
    }
    
    class OMDBApi {
        
        class func createURLForMovieDetails(with id: String) -> String? {
            return omdbAPIBaseURL + "&i=\(id)"
        }
        
        class func createURLForMovieSearch(with title: String, page: Int? = nil) -> String? {
            let url = omdbAPIBaseURL + "&s=\(title)"
            guard let page = page, page > 0 else {
                return url
            }
            return url + "&page=\(page)"
        }
        
    }
    
    private static var posterAPIBaseURL: String {
        return "\(Defaults.posterAPIBaseURL.rawValue)?apikey=\(Defaults.apiKey.rawValue)&h=600"
    }
    
    class PosterAPI {
        
        class func createURLForMoviePoster(withId id: String) -> String? {
            return posterAPIBaseURL + "&i=\(id)"
        }
        
    }
    
}
