//
//  CustomErrorCodes.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 16/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation

enum CustomErrorCode: Int {
    case unknow = -1
    case noURLParameters = -998
    case couldNotConnectToHost = -999
    case objectMapperParse = -6004
    case invalidParameters = -997
    case couldNotDeleteObject = -996
    case userIsNotLoggedIn = -1111
    case duplicatedDatabaseEntry = -1234
}
