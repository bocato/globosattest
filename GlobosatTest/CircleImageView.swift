//
//  CircleImageView.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit

@IBDesignable class CircleImageView: UIImageView {

    // MARK: - Properties
    @IBInspectable open var borderColor: UIColor?
    @IBInspectable open var borderWidth: CGFloat?
    @IBInspectable open var shadow: Bool = false
    
    // MARK: - Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        reconfigureLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        reconfigureLayout()
    }
    
    func reconfigureLayout() {
        self.layer.borderColor = borderColor?.cgColor ?? UIColor.clear.cgColor
        self.layer.borderWidth = borderWidth ?? 0
        self.layer.cornerRadius = self.bounds.size.height/2
        self.clipsToBounds = true
        if shadow {
            self.layer.shadowOffset = CGSize(width: -15, height: 20)
            self.layer.shadowRadius = 5
            self.layer.shadowOpacity = 0.5
            self.layer.shadowColor = UIColor.black.cgColor
        }
    }
    

}
