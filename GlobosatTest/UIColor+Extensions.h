//
//  UIColor+Extensions.h
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extensions)

+ (UIColor *)colorWithHex:(unsigned)hex;
+ (UIColor *)colorWithHexString:(NSString *)hex;
+ (UIColor *)randomColor;

@end
