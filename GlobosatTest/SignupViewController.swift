//
//  SignupViewController.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 16/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SkyFloatingLabelTextField
import SnapKit

protocol SignupViewControllerDelegate {
    func successfullySignedUpWithUser(_ user: User)
}

class SignupViewController: UIViewController {
    
    // MARK: - Properties
    var delegate: SignupViewControllerDelegate?
    var showDismissButton: Bool = false
    
    // MARK: - View Elements
    @IBOutlet weak var displayNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    // MARK: - Instantiation
    static func instantiateNew() ->  SignupViewController {
        let storyboard = UIStoryboard(name: "Signup", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        return controller
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFieldsAcessoryView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureDismissButtonIfNeeded()
    }
    
    // MARK: - Configuration
    func configureTextFieldsAcessoryView() {
        let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        toolbar.barStyle = .default
        
        let cancelButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissKeyboard))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let signInButtonItem = UIBarButtonItem(title: "Signup", style: .done, target: self, action: #selector(createUserAction))
        
        toolbar.items = [cancelButtonItem, flexibleSpace, signInButtonItem]
        toolbar.sizeToFit()
        
        self.displayNameTextField.inputAccessoryView = toolbar
        self.emailTextField.inputAccessoryView = toolbar
        self.passwordTextField.inputAccessoryView = toolbar
    }
    
    func configureDismissButtonIfNeeded() {
        if showDismissButton {
            let dismissBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissViewController))
            self.navigationItem.rightBarButtonItem = dismissBarButtonItem
        }
    }
    
    // MARK: - Validations
    func getValidatedDisplayName() -> (isValid: Bool, value: String?) {
        guard let displayName = displayNameTextField.text, !displayName.isEmpty else {
            displayNameTextField.errorMessage = "Invalid display name."
            return (false, nil)
        }
        return (true, displayName)
    }
    
    func getValidatedEmail() -> (isValid: Bool, value: String?) {
        guard let email = emailTextField.text, !email.isEmpty && email.isValidEmail else {
            emailTextField.errorMessage = "Invalid email."
            return (false, nil)
        }
        return (true, email)
    }
    
    func getValidatedPassword() -> (isValid: Bool, value: String?) {
        guard let password = passwordTextField.text, !password.isEmpty || password.characters.count < 6 else {
            passwordTextField.errorMessage = "Invalid password."
            return (false, nil)
        }
        return (true, password)
    }
    
    // MARK: - Service Calls
    func createUser(withDisplayName validatedDisplayName: (isValid: Bool, value: String?), email validatedEmail: (isValid: Bool, value: String?), password validatedPassword: (isValid: Bool, value: String?)) {
        
        guard let displayName = validatedDisplayName.value, let email = validatedEmail.value, let password = validatedPassword.value,
            validatedDisplayName.isValid && validatedEmail.isValid && validatedPassword.isValid else {
            return
        }
        
        SwiftyLoadingView.show(in: self, withText: "Creating account...", background: true)
        UserService.createUser(withDisplayName: displayName, email: email, password: password, success: { (user) in
            guard let user = user else {
                AlertHelper.showAlert(in: self, with: "Error", message: ErrorMessages.unexpected.rawValue)
                return
            }
            self.handle(user: user)
        }, failure: { (error) in
            AlertHelper.showAlert(in: self, with: "Error", message: error?.localizedDescription ?? ErrorMessages.unexpected.rawValue)
        }, completion: {
            SwiftyLoadingView.hide(for: self)
        })
        
    }
    
    // MARK: - Handle User
    func handle(user: User, completion: (() -> ())? = nil) {
        AlertHelper.showAlert(in: self, with: "Success", message: "\(user.email!) is now registered!", action: UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: { 
                self.delegate?.successfullySignedUpWithUser(user)
            })
        }))
    }
    
    // MARK: Actions
    func createUserAction(){
        createUser(withDisplayName: self.getValidatedDisplayName(), email: self.getValidatedEmail(), password: self.getValidatedPassword())
    }
    
    func dismissKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Button Actions
    @IBAction func loginButtonDidTouchUpInside(_ sender: Any) {
        createUserAction()
    }
    
    @IBAction func dismissButtonDidReceiveTouchUpInside(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Extensions
extension SignupViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let skyFloatingLabelTextField = textField as? SkyFloatingLabelTextField {
            skyFloatingLabelTextField.errorMessage = nil
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

