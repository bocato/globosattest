//
//  UIViewController+Extension.h
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extension)
+ (UIViewController *)topViewController;
+ (UIViewController *)topBarViewController;
+ (UIViewController *)currentPresentingViewController;
@end
