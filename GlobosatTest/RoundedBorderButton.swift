//
//  RoundedBorderButton.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit

class RoundedBorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layoutIfNeeded()
        layer.cornerRadius = 6;
        clipsToBounds = true;
    }
    
}
