
//
//  UserService.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class UserService {

    // MARK: Create 
    static func createUser(withDisplayName displayName: String!, email: String!, password: String!, success: @escaping (User?) -> (), failure: @escaping (Error?) -> (), completion: (() -> ())? = nil){
        
        Auth.auth().createUser(withEmail: email!, password: password) { (user, error) in
            
            if let error = error {
                failure(error as NSError)
            }
            
            if let user = user {
                
                let changeRequest = user.createProfileChangeRequest()
                changeRequest.displayName = displayName
                
                changeRequest.commitChanges() { (error) in
                    if let error = error {
                        failure(error as NSError)
                    }
                    else {
                        success(user)
                    }
                    completion?()
                }
                
                
            }
            completion?()
        }
        
    }
    
}
