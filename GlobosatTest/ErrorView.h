//
//  ErrorView.h
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 16/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ErrorView;

#pragma mark - Delegate
@protocol ErrorViewDelegate <NSObject>
@optional
- (void)showErrorView:(ErrorView *)errorView;
- (void)hideErrorView:(ErrorView *)errorView;
@end

@interface ErrorView : UIControl

#pragma mark - Properties
@property (strong, nonatomic) id<ErrorViewDelegate> delegate;

#pragma mark - View Behavior
- (void)hideView;
- (void)showView;

#pragma mark - Configuration
+ (instancetype)configuredInstanceFor:(UIView *)view withDelegate:(id<ErrorViewDelegate>)delegate action:(SEL)action;

@end
