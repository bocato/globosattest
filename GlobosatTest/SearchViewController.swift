//
//  SearchViewController.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit

private enum CellIdentifiers: String {
    case SimpleMovieTableViewCell = "SimpleMovieTableViewCell"
}

private enum SegueIdentifiers: String {
    case MovieDetails = "MovieDetailsSegue"
}

class SearchViewController: UIViewController {

    // MARK: - View Elements
    let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tapToStartView: UIStackView!
    @IBOutlet weak var noResultsView: UIStackView!
    var tableViewFooterActivityIndicator: UIActivityIndicatorView!
    // MARK: - View Properties
    var searchResult: [Movie]? {
        didSet {
            self.tapToStartView.isHidden = true
            self.noResultsView.isHidden = true
            tableView.reloadData()
        }
    }
    var lastSearchString: String?
    var totalResults = 0
    var currentPage = 0
    var isFirstPage: Bool {
        return currentPage == 0
    }
    var isLastPage: Bool {
        guard let searchResult = searchResult else { return false }
        return searchResult.count == totalResults
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearchController()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedRowIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedRowIndexPath, animated: true)
        }
    }
    
    // MARK: - Configuration
    func configureSearchController() {
        searchController.searchBar.delegate = self
    }
    
    func configureTableView() {
        tableView.tableHeaderView = searchController.searchBar
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: CellIdentifiers.SimpleMovieTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.SimpleMovieTableViewCell.rawValue)
        configureTableViewFooterActivityIndicator()
    }
    
    func configureTableViewFooterActivityIndicator(){
        tableViewFooterActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        tableViewFooterActivityIndicator.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30)
        tableView.tableFooterView = tableViewFooterActivityIndicator
        tableViewFooterActivityIndicator.isHidden = true
    }
    
    // MARK: - Helpers
    func showLoader() {
        if isFirstPage {
            LoadingView.show(in: view, withLoaderStyle: .gray)
        }
        else {
            tableViewFooterActivityIndicator.isHidden = false
            tableViewFooterActivityIndicator.startAnimating()
        }
    }
    
    func hideLoader() {
        if self.isFirstPage {
            LoadingView.hide(for: view)
        }
        else{
            tableViewFooterActivityIndicator.stopAnimating()
            tableViewFooterActivityIndicator.isHidden = true
        }
    }
    
    func resetSearchData() {
        currentPage = 0
        totalResults = 0
        searchResult = nil
        lastSearchString = nil
    }
    
    // MARK: - Api Requests
    func searchMovieWithTitle(_ title: String?) {
        
        lastSearchString = title
        
        if let title = title, !title.isEmpty {
            showLoader()
            MovieService.searchMovies(withTitle: title, page: currentPage, onSuccess: { (response) in
                guard let searchResult = response?.search, let totalResults = response?.totalResults else {
                    self.resetSearchData()
                    self.noResultsView.isHidden = false
                    self.tableView.reloadData()
                    return
                }
                
                self.totalResults = Int(totalResults)!
                
                if self.isFirstPage {
                    self.searchResult = searchResult
                }
                else {
                    self.searchResult?.append(contentsOf: searchResult)
                }
            }, onFailure: { (error) in
                AlertHelper.showAlert(in: self, with: "Error", message: error?.localizedDescription ?? ErrorMessages.unknown.rawValue)
            }, onCompletion: {
                self.hideLoader()
            })
        }
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.MovieDetails.rawValue {
            if let selectedIndexPath = sender as? IndexPath, let selectedMovie = searchResult?[selectedIndexPath.row], let imdbId = selectedMovie.imdbId, let destination = segue.destination as? MovieDetailsViewController {
                destination.title = selectedMovie.title ?? "Movie Details"
                destination.imdbId = imdbId
            }
        }
    }

}

// MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResult?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SimpleMovieTableViewCell.rawValue, for: indexPath) as! SimpleMovieTableViewCell
        cell.configureWithMovie(searchResult?[indexPath.row])
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueIdentifiers.MovieDetails.rawValue, sender: indexPath)
    }
    
}

// MARK: - UIScrollViewDelegate
extension SearchViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let searchResult = searchResult, let lastSearchString = lastSearchString else { return }
        if self.tableView.tableViewScrollDidReachBottom, searchResult.count < totalResults {
            currentPage += 1
            searchMovieWithTitle(lastSearchString)
        }
    }
    
}

// MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let searchText = searchBar.text, searchText.characters.count > 0 {
            resetSearchData()
            searchMovieWithTitle(searchBar.text)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
}
