//
//  ErrorMessages.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation

enum ErrorMessages: String {
    case unexpected = "An unexpected error has occoured."
    case unknown = "Unknown error"
    case objectMapperParse = "Object Mapper parser error"
    case noURLParameters = "Didn't you forgot to use the URL parameters on the request?"
    case couldNotConnectToHost = "Could not connect to host: "
    case notFound = "Error 404: not found."
    case tryAgain = "An error has occoured, please try again."
    case wishToTryAgain = "An error has occoured, would you like to try again?"
    case serverFailure = "Server error. Try again later."
    case noAvailableResults = "No available results."
    case networkFailure = "Network error."
    case invalidParameters = "Invalid parameters."
    case couldNotDeleteObject = "Could not delete object."
    case userIsNotLoggedIn = "Login needed."
    case favoritesListNotFound = "Favorites not found for user."
    case duplicatedDatabaseEntry = "Duplicated database entry."
}
