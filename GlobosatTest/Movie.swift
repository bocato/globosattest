//
//  Movie.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import Foundation
import ObjectMapper
import FirebaseDatabase

class Movie: Mappable {
    
    // MARK: - Properties
    var firebaseReference: DatabaseReference? = nil
    var title: String?
    var year: String?
    var rated: String?
    var released: String?
    var runtime: String?
    var genre: String?
    var director: String?
    var writer: String?
    var actors: String?
    var plot: String?
    var language: String?
    var country: String?
    var awards: String?
    var poster: String?
    var ratings: [Rating]?
    var metascore: String?
    var imdbRating: String?
    var imdbVotes: String?
    var imdbId: String?
    var type: String?
    var dvd: String?
    var boxOffice: String?
    var production: String?
    var website: String?
    var response: String?
    
    // MARK: - Initialization
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["Title"]
        year <- map["Year"]
        rated <- map["Rated"]
        released <- map["Released"]
        runtime <- map["Runtime"]
        genre <- map["Genre"]
        director <- map["Director"]
        writer <- map["Writer"]
        actors <- map["Actors"]
        plot <- map["Plot"]
        language <- map["Language"]
        country <- map["Country"]
        awards <- map["Awards"]
        poster <- map["Poster"]
        ratings <- map["Ratings"]
        metascore <- map["Metascore"]
        imdbRating <- map["imdbRating"]
        imdbVotes <- map["imdbVotes"]
        imdbId <- map["imdbID"]
        type <- map["Type"]
        dvd <- map["DVD"]
        boxOffice <- map["BoxOffice"]
        production <- map["Production"]
        website <- map["Website"]
        response <- map["Response"]
    }
    
    required init(snapshot: DataSnapshot) {
        firebaseReference = snapshot.ref
        if let firebaseJson = snapshot.value as? [String: Any] {
            mapping(map: Map(mappingType: .fromJSON, JSON: firebaseJson))
        }
    }
    
}

extension Movie: Equatable {
    
    static func ==(lhs: Movie, rhs: Movie) -> Bool {
        return lhs.imdbId == rhs.imdbId
    }
    
}
