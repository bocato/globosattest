//
//  MovieDetailsViewController.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import AlamofireImage
import Firebase
import FirebaseAuth

private enum CellIdentifiers: String {
    case MovieDetailsTableViewCell = "MovieDetailsTableViewCell"
}

class MovieDetailsViewController: UIViewController, ErrorViewDelegate {
    
    // MARK: - View Elements
    @IBOutlet weak var tableView: UITableView!
    var errorView: ErrorView!
    
    // MARK: - View Properties
    var imdbId: String!
    var movie: Movie?
    var currentUser: User? = nil
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureErrorView()
        loadViewData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addAuthListeners()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(self)
    }
    
    // MARK: - Configuration
    func addAuthListeners() {
        Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user != self.currentUser {
                self.currentUser = user
            }
            self.configureNavigationBar()
        })
    }
    
    func configureNavigationBar() {
        if let currentUser = currentUser {
            FavoritesService.recordNodeFor(currentUser, imdbId: imdbId).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    self.configureRemoveFromFavoritesBarButtonItem()
                }
                else {
                   self.configureAddToFavoritesBarButtonItem()
                }
            })
        } else {
            configureAddToFavoritesBarButtonItem()
        }
    }
    
    func configureAddToFavoritesBarButtonItem() {
        let addToFavoritesBarButtonItem = UIBarButtonItem(image: UIImage(named: "add-bookmarks"), style: .plain, target: self, action: #selector(addToFavorites))
        self.navigationItem.rightBarButtonItem = addToFavoritesBarButtonItem
    }
    
    func configureRemoveFromFavoritesBarButtonItem() {
        let removeFromFavoritesBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(removeFromFavorites))
        self.navigationItem.rightBarButtonItem = removeFromFavoritesBarButtonItem
    }
    
    func configureErrorView() {
        errorView = ErrorView(frame: view.frame)
        view.addSubview(errorView)
        errorView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(self.view)
        }
        errorView.delegate = self
        errorView.addTarget(self, action: #selector(reloadViewData), for: .touchUpInside)
    }
    
    // MARK: - Api Requests
    func loadViewData() {
        if movie == nil {
            LoadingView.show(in: view, withLoaderStyle: .gray)
            MovieService.getDetailsForMovie(withId: imdbId, onSuccess: { (response) in
                guard let response = response else {
                    self.errorView.show()
                    return
                }
                self.movie = response
                self.tableView.reloadData()
            }, onFailure: { (error) in
                self.errorView.show()
                AlertHelper.showAlert(in: self, with: "Error", message: error?.localizedDescription ?? ErrorMessages.unknown.rawValue)
            }, onCompletion: {
                LoadingView.hide(for: self.view)
            })
        }
        else {
            self.tableView.reloadData()
        }
    }
    
    func reloadViewData() {
        errorView.hide()
        loadViewData()
    }
    
    func addToFavorites() {
        SwiftyLoadingView.show(in: self, withText: "Saving...", background: true)
        FavoritesService.saveMovie(movie, success: { (response) in
//            AlertHelper.showAlert(in: self, with: "Success", message: "\(response.title!) added to favorites!")
            self.configureRemoveFromFavoritesBarButtonItem()
        }, failure: { (error) in
            
            guard let error = error else {
                AlertHelper.showAlert(in: self, with: "Error", message: ErrorMessages.unknown.rawValue)
                return
            }
            
            if error.code == CustomErrorCode.userIsNotLoggedIn.rawValue {
                self.showLoginDialog()
            }
            else {
                AlertHelper.showAlert(in: self, with: "Error", message: error.localizedDescription)
            }
            
        }, completion:  {
            SwiftyLoadingView.hide(for: self)
        })
    }
    
    func removeFromFavorites() {
        SwiftyLoadingView.show(in: self, withText: "Deleting...", background: true)
        FavoritesService.delete(movie, success: { (response) in
//        AlertHelper.showAlert(in: self, with: "Success", message: "\(response.title!) removed from favorites!")
            self.configureAddToFavoritesBarButtonItem()
        }, failure: { (error) in
            
            guard let error = error else {
                AlertHelper.showAlert(in: self, with: "Error", message: ErrorMessages.unknown.rawValue)
                return
            }
            
            if error.code == CustomErrorCode.userIsNotLoggedIn.rawValue {
                self.showLoginDialog()
            }
            else {
                AlertHelper.showAlert(in: self, with: "Error", message: error.localizedDescription)
            }
            
        }, completion:  {
            SwiftyLoadingView.hide(for: self)
        })
    }
    
    func showLoginDialog(){
        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            let loginController = LoginViewController.instantiateNew()
            loginController.delegate = self
            let navigationController = UINavigationController(rootViewController: loginController)
            self.present(navigationController, animated: true, completion: nil)
        })
        AlertHelper.showAlert(in: self, with: "Error: Login needed", message: "Would you like to login?", yesAction: yesAction)
    }
    
}

// MARK: - UITableViewDataSource
extension MovieDetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movie != nil ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.MovieDetailsTableViewCell.rawValue, for: indexPath) as! MovieDetailsTableViewCell
        cell.configureWithMovie(movie!)
        cell.delegate = self
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension MovieDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.size.height
    }
    
}

// MARK: - LoginViewControllerDelegate
extension MovieDetailsViewController: LoginViewControllerDelegate {
    func successfullyLoggedInWithUser(_ user: User) {
        AlertHelper.showAlert(in: self, with: "Login", message: "\(user.email!) successfully logged in.")
    }
}

// MARK: - MovieDetailsTableViewCellDelegate
extension MovieDetailsViewController: MovieDetailsTableViewCellDelegate {
    
    func websiteButtonDidReceiveTouchUpInside(in cell: MovieDetailsTableViewCell) {
        // TODO: Implement
        debugPrint("websiteButtonDidReceiveTouchUpInside")
    }
}
