//
//  FavoritesViewController.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Alamofire
import AlamofireImage
import NYTPhotoViewer

private enum CellIdentifiers: String {
    case SimpleMovieTableViewCell = "SimpleMovieTableViewCell"
    case PosterCollectionViewCell = "PosterCollectionViewCell"
}

private enum SegueIdentifiers: String {
    case MovieDetails = "MovieDetailsSegue"
}

class FavoritesViewController: UIViewController {
    
    // MARK: - View Elements
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewLayout: UPCarouselFlowLayout!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favoritesContentView: UIView!
    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var loginOrSignupView: UIView!
    
    // MARK: - View Properties
    var currentUser: User? = nil
    var favorites: [Movie]? {
        didSet {
            collectionView.reloadData()
            tableView.reloadData()
            if let favorites = favorites {
                favoritesContentView.isHidden = !(favorites.count > 0)
            }
        }
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addAuthListeners()
        if let selectedRowIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedRowIndexPath, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(self)
    }
    
    // MARK: - Configuration 
    func addAuthListeners() {
        Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user != nil || user != self.currentUser {
                self.currentUser = user
                guard let displayName = user?.displayName, !displayName.isEmpty else {
                    self.displayNameLabel.text = ""
                    return
                }
                self.displayNameLabel.text = "\(displayName)'s Favorites"
            }
            self.configureRealtimeData()
            self.configureNavigationBar()
            self.loginOrSignupView.isHidden = user != nil
        })
    }
    
    func configureTableView() {
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: CellIdentifiers.SimpleMovieTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.SimpleMovieTableViewCell.rawValue)
    }
    
    func configureCollectionView(){
        
        var itemSize: CGSize {
            let width = 140
            let height = 180
            return CGSize(width: width, height: height)
        }
        
        collectionViewLayout.sideItemShift = 0.1
        collectionViewLayout.itemSize = itemSize
        collectionViewLayout.minimumInteritemSpacing = 10
    }
    
    func configureRealtimeData() {
        LoadingView.show(in: view)
        if let currentUser = currentUser {
            Database.database().reference().child(FirebaseNode.Favorites.rawValue).child(currentUser.uid).observe(.value, with: { (snapshot) in
                var movies = [Movie]()
                for item in snapshot.children {
                    let user = Movie(snapshot: item as! DataSnapshot)
                    movies.append(user)
                }
                
                if let favorites = self.favorites, favorites.count > movies.count {
                    self.collectionView.performBatchUpdates({
                        let removedItems = favorites.filter({ (favorite) -> Bool in return !movies.contains(where: { $0.imdbId == favorite.imdbId } ) })
                        for item in removedItems {
                            if let removedIndex = favorites.index(of: item) {
                                self.collectionView.deleteItems(at: [IndexPath(item: removedIndex, section: 0)])
                            }
                        }
                        self.favorites = movies
                    }, completion: nil)
                }
                else {
                    self.favorites = movies
                }
            })
        }
        else {
            debugPrint("Login needed.")
            favorites = nil
            favoritesContentView.isHidden = true
            loginOrSignupView.isHidden = false
        }
        LoadingView.hide(for: view)
    }
    
    func configureNavigationBar() {
        if Auth.auth().currentUser != nil {
            let logoutBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(doLogout))
            self.navigationItem.rightBarButtonItem = logoutBarButtonItem
        }
        else {
            let loginBarButtonItem = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(presentLogin))
            self.navigationItem.rightBarButtonItem = loginBarButtonItem
        }
    }
    
    // MARK: - API Requests
    func doLogout() {
        AlertHelper.showAlert(in: self, with: "Logout", message: "Do you really want to logout?", yesAction: UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            SwiftyLoadingView.show(in: self, withText: "Loging out...", background: true)
            LoginService.doLogout(success: { (user) in
                AlertHelper.showAlert(in: self, with: "Logout", message: "\(user.email!) successfully logged out.")
                self.currentUser = nil
                self.configureNavigationBar()
                self.loginOrSignupView.isHidden = false
            }, failure: { (error) in
                AlertHelper.showAlert(in: self, with: "Error", message: error?.localizedDescription ?? ErrorMessages.unexpected.rawValue)
            }, completion: { 
                SwiftyLoadingView.hide(for: self)
            })
        }))
    }
    
    // MARK: - Actions
    func presentLogin() {
        let loginController = LoginViewController.instantiateNew()
        loginController.delegate = self
        let navigationController = UINavigationController(rootViewController: loginController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func presentSignup() {
        let signupController = SignupViewController.instantiateNew()
        signupController.delegate = self
        signupController.showDismissButton = true
        let navigationController = UINavigationController(rootViewController: signupController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    // MARK: - Button Actions
    @IBAction func loginButtonDidReceiveTouchUpInside(_ sender: Any) {
        presentLogin()
    }
    
    @IBAction func signupButtonDidReceiveTouchUpInside(_ sender: Any) {
        presentSignup()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.MovieDetails.rawValue {
            if let selectedIndexPath = sender as? IndexPath, let selectedMovie = favorites?[selectedIndexPath.row], let imdbId = selectedMovie.imdbId, let destination = segue.destination as? MovieDetailsViewController {
                destination.title = selectedMovie.title ?? "Movie Details"
                destination.imdbId = imdbId
                destination.movie = selectedMovie
            }
        }
    }
    
}


// MARK: - LoginViewControllerDelegate
extension FavoritesViewController: LoginViewControllerDelegate {
    
    func successfullyLoggedInWithUser(_ user: User) {
        //AlertHelper.showAlert(in: self, with: "Login", message: "\(user.email!) successfully logged in.")
    }
    
}

// MARK: - SignupViewControllerDelegate
extension FavoritesViewController: SignupViewControllerDelegate {
    
    func successfullySignedUpWithUser(_ user: User) {
        //AlertHelper.showAlert(in: self, with: "Login", message: "\(user.email!) successfully signed up and logged in.")
    }
    
}

// MARK: - UITableViewDataSource
extension FavoritesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favorites?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SimpleMovieTableViewCell.rawValue, for: indexPath) as! SimpleMovieTableViewCell
        cell.configureWithMovie(favorites?[indexPath.row])
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension FavoritesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let removeFromFavoritesAction = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            if let movie = self.favorites?[indexPath.row] {
                FavoritesService.delete(movie, success: { (movie) in
                    debugPrint("Removed \(movie.title!)")
                }, failure: { (error) in
                    AlertHelper.showAlert(in: self, with: "Error", message: error?.localizedDescription ?? ErrorMessages.unknown.rawValue)
                }, completion: {
                    debugPrint("FavoritesService.delete - Completion")
                })
            }
        }
        
        return [removeFromFavoritesAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueIdentifiers.MovieDetails.rawValue, sender: indexPath)
    }
    
}

// MARK: - UICollectionViewDataSource
extension FavoritesViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.favorites?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.PosterCollectionViewCell.rawValue, for: indexPath) as! PosterCollectionViewCell
        cell.delegate = self
        cell.configureWithMovie(favorites?[indexPath.row])
        return cell
    }
    
}

// MARK: - PosterCollectionViewCellDelegate
extension FavoritesViewController: PosterCollectionViewCellDelegate {
    
    func imageViewDidReceiveTap(_ imageView: UIImageView, on cell: PosterCollectionViewCell) {
        if let image = imageView.image {
            let photos = [Photo(image: image)]
            let photosViewController = NYTPhotosViewController(photos: photos)
            self.present(photosViewController, animated: true, completion: nil)
        }
    }
    
}


