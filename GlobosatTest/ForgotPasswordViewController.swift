//
//  ForgotPasswordViewController.swift
//  GlobosatTest
//
//  Created by Eduardo Sanches Bocato on 15/07/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var sendButtonContainer: UIView!
    var startingSendButtonContainerY: CGFloat = 0.0
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNotifications()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Configuration
    func configureNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowWithNotification), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideWithNotification), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: - Keyboard Hide/Show/Changes Configuration
    func keyboardWillShowWithNotification(notification: Notification){
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let bottomInset = keyboardFrame.height != 0 ? keyboardFrame.height : 0
        
        self.startingSendButtonContainerY = self.sendButtonContainer.frame.origin.y
        let newLoginBurronContainerFrameY = self.startingSendButtonContainerY - bottomInset
        let newLoginButtonContainerFrame = CGRect(x: self.sendButtonContainer.frame.origin.x, y: newLoginBurronContainerFrameY, width: self.sendButtonContainer.frame.size.width, height: self.sendButtonContainer.frame.size.height)
        UIView.animate(withDuration: 0.25) {
            self.sendButtonContainer.frame = newLoginButtonContainerFrame
        }
    }
    
    func keyboardWillHideWithNotification() {
        let newLoginButtonContainerFrame = CGRect(x: self.sendButtonContainer.frame.origin.x, y: self.startingSendButtonContainerY, width: self.sendButtonContainer.frame.size.width, height: self.sendButtonContainer.frame.size.height)
        UIView.animate(withDuration: 0.25) {
            self.sendButtonContainer.frame = newLoginButtonContainerFrame
        }
    }
    
    // MARK: - Validations
    func getValidatedEmail() -> (isValid: Bool, value: String?) {
        guard let email = emailTextField.text, !email.isEmpty && email.isValidEmail else {
            emailTextField.errorMessage = "E-Mail inválido."
            return (false, nil)
        }
        return (true, email)
    }
    
    // MARK: - Service Calls
    func resetPassword(withEmail validatedEmail: (isValid: Bool, value: String?)) {
        
        guard let email = validatedEmail.value, validatedEmail.isValid else {
            return
        }
        
        SwiftyLoadingView.show(in: self, background: true)
        LoginService.resetPassword(withEmail: email, success: {
            AlertHelper.showAlert(in: self, with: "Success", message: "E-Mail recover instructions sent to \(email)")
        }, failure: { (error) in
            AlertHelper.showAlert(in: self, with: "Error", message: error?.localizedDescription ?? ErrorMessages.unexpected.rawValue)
        }, completion: {
            SwiftyLoadingView.hide(for: self)
        })
    }
    
    // MARK: Button Actions
    @IBAction func sendButtonDidTouchUpInside(_ sender: Any) {
        self.emailTextField.resignFirstResponder()
        resetPassword(withEmail: getValidatedEmail())
    }
    
}

// MARK: - Extensions
extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let skyFloatingLabelTextField = textField as? SkyFloatingLabelTextField {
            skyFloatingLabelTextField.errorMessage = nil
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
