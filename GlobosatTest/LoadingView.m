
//
//  LoadingView.m
//
//  Created by Eduardo Sanches Bocato on 17/06/17.
//  Copyright © 2017 Bocato. All rights reserved.
//

#import "LoadingView.h"
#import "Masonry.h"

//static CGFloat const kBackgroundViewAlpha = 0.9;

#pragma mark - Delegate Default Implemetation
@implementation NSObject(LoadingViewDelegate)

- (void)checkIfTheClassConformsWithLoadingViewDelegate {
    if (![self conformsToProtocol:@protocol(LoadingViewDelegate)]){
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must conform to protocol LoadingViewDelegate to access this method."] userInfo:nil];
    }
}

- (void)showLoadingView:(LoadingView *)loadingView {
    [self checkIfTheClassConformsWithLoadingViewDelegate];
    [loadingView show];
}

- (void)hideLoadingView:(LoadingView *)loadingView {
    [self checkIfTheClassConformsWithLoadingViewDelegate];
    [loadingView hide];
}

@end


@interface LoadingView ()
#pragma mark - View Elements
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation LoadingView

#pragma mark - Instantiation
- (instancetype)init {
    if (self = [super init]) {
        [self configureViewElements];
        return self;
    }
    return nil;
}

+ (instancetype)instanciateNewInView:(UIView*)view withBackgroundColor:(UIColor*)backgroundColor {
    LoadingView *loadingView = [LoadingView new];
    loadingView.frame = view.frame;
    backgroundColor ? [loadingView setBackgroundColor:backgroundColor] : [loadingView setBackgroundColor:[UIColor clearColor]];
    [view addSubview:loadingView];
    [view bringSubviewToFront:loadingView];
    [loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.top.equalTo(view);
    }];
    return loadingView;
}

+ (void)showInView:(UIView *)view withLoaderStyle:(UIActivityIndicatorViewStyle)style {
    LoadingView *loadingView = [LoadingView instanciateNewInView:view withBackgroundColor:nil];
    loadingView.activityIndicator.activityIndicatorViewStyle = style;
    [loadingView show];
}


+ (void)showInView:(UIView *)view {
    LoadingView *loadingView = [LoadingView instanciateNewInView:view withBackgroundColor:nil];
    [loadingView show];
}

+ (NSPredicate *)loadingViewPredicate {
    return [NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
        return [object isKindOfClass:LoadingView.class];
    }];
}

+ (void)hideForView:(UIView *)view {
    LoadingView *currentVisibleLoadingView = [view.subviews filteredArrayUsingPredicate:[LoadingView loadingViewPredicate]].firstObject;
    if (currentVisibleLoadingView) {
        [currentVisibleLoadingView hide];
        [currentVisibleLoadingView removeFromSuperview];
    }
}

+ (BOOL)containsLoadingViewAsSubviewWithView:(UIView*)view {
    BOOL contains = false;
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:LoadingView.class]) {
            contains = true;
        }
    }
    return contains;
}

- (void)configureActivityIndicator {
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    self.activityIndicator.color = [UIColor lightGrayColor];
    [self addSubview:self.activityIndicator];
}

- (void)makeConstraints {
    [self.activityIndicator mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.mas_equalTo(self);
        make.height.width.mas_equalTo(50);
    }];
}

- (void) configureViewElements {
    self.backgroundColor = [UIColor whiteColor];
    self.hidden = YES;
    [self configureActivityIndicator];
    [self makeConstraints];
}

- (void)configureForView:(UIView *)view {
    [self mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.left.right.equalTo(view);
    }];
}

#pragma mark - View Behavior
- (void)hide {
    self.alpha = 1.0;
    [UIView animateWithDuration:0.35 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.activityIndicator stopAnimating];
            self.hidden = YES;
        }
    }];
}

- (void)show {
    self.hidden = NO;
    self.alpha = 0.0;
    [self.activityIndicator startAnimating];
    [UIView animateWithDuration:0.35 animations:^{
        self.alpha = 1.0;
    }];
}

@end
